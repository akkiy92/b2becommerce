package com.ecom.b2b.supplier.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ecom.b2b.core.model.BaseEntity;

@Entity
@Table(name = "supplier")
public class Supplier extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6705449985586037890L;

	@Column()
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(LocalDateTime modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	@Column()
	private Boolean isActive;

	@Column()
	private String createdBy;

	@Column()
	private String modifiedBy;

	@Column()
	private LocalDateTime createdAt;
	
	@Column()
	private LocalDateTime modifiedAt;
}
