package com.ecom.b2b.supplier.dto;

import com.ecom.b2b.core.dto.BaseDTO;
import com.ecom.b2b.supplier.model.Supplier;

public class SupplierDTO extends BaseDTO {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	private Boolean isActive;
	
	public SupplierDTO() {
		super();
	}
	
	public SupplierDTO(Supplier supplier) {
		super();
		if(supplier != null) {
			this.setId(supplier.getId());
			this.setName(supplier.getName());
			this.setIsActive(supplier.getIsActive());
		}
	}

}
