package com.ecom.b2b.brand.dto;

import com.ecom.b2b.brand.model.Brand;
import com.ecom.b2b.core.dto.BaseDTO;

public class BrandDTO extends BaseDTO {

	public BrandDTO() {
		super();
	}

	public BrandDTO(Brand entity) {
		if(entity != null) {
			this.setId(entity.getId());
			this.setIsActive(entity.getIsActive());
			this.setName(entity.getName());
		}
	}

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	private Boolean isActive;

}
