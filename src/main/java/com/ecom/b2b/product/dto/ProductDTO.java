package com.ecom.b2b.product.dto;

import java.math.BigDecimal;

import com.ecom.b2b.brand.dto.BrandDTO;
import com.ecom.b2b.core.dto.BaseDTO;
import com.ecom.b2b.product.model.Product;
import com.ecom.b2b.supplier.dto.SupplierDTO;

public class ProductDTO extends BaseDTO {
	private String name;

	private String sku;

	private Integer size;

	private String color;

	private BigDecimal price;

	private Integer availableQty;

	private Boolean isActive;

	private SupplierDTO supplier;

	private BrandDTO brand;

	public ProductDTO() {
		super();
	}

	public ProductDTO(Product entity) {
		if(entity != null) {
			this.setId(entity.getId());
			this.setName(entity.getName());
			this.setSku(entity.getSku());
			this.setAvailableQty(entity.getAvailableQty());
			this.setBrand(new BrandDTO(entity.getBrand()));
			this.setSupplier(new SupplierDTO(entity.getSupplier()));
			this.setColor(entity.getColor());
			this.setPrice(entity.getPrice());
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getAvailableQty() {
		return availableQty;
	}

	public void setAvailableQty(Integer availableQty) {
		this.availableQty = availableQty;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public SupplierDTO getSupplier() {
		return supplier;
	}

	public void setSupplier(SupplierDTO supplier) {
		this.supplier = supplier;
	}

	public BrandDTO getBrand() {
		return brand;
	}

	public void setBrand(BrandDTO brand) {
		this.brand = brand;
	}
}
