package com.ecom.b2b.product.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecom.b2b.product.dto.ProductDTO;
import com.ecom.b2b.product.dto.ProductFilter;
import com.ecom.b2b.product.service.ProductService;

@RestController
@RequestMapping(path = { "/v1/product" })
public class ProductController {

	@Autowired
	private ProductService _productService;

	@PostMapping()
	public ProductDTO createProduct(@Valid @RequestBody ProductDTO dto) {
		return this._productService.create(dto);
	}

	@PutMapping(value = "/{id}")
	public ProductDTO updateProduct(@PathVariable(name = "id") String id, @Valid @RequestBody ProductDTO dto) {
		dto.setId(id);
		return this._productService.update(dto);
	}

	@GetMapping(value = "/{id}")
	public ProductDTO getProduct(@PathVariable(name = "id") String id) {
		return this._productService.getById(id);
	}

	@GetMapping(value = "/sku/{sku}")
	public ProductDTO getProductBySku(@PathVariable(name = "sku") String sku) {
		return this._productService.getBySku(sku);
	}

	@GetMapping(value = "/list", produces="application/json")
	public Page<ProductDTO> getByFilter(ProductFilter filter) {
		return this._productService.getByFilter(filter);
	}

}
