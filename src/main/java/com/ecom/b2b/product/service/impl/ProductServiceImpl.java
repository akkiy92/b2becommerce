package com.ecom.b2b.product.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.ecom.b2b.core.service.AbstractService;
import com.ecom.b2b.product.dao.ProductDAO;
import com.ecom.b2b.product.dao.ProductDAOSpecificationBuilder;
import com.ecom.b2b.product.dto.ProductDTO;
import com.ecom.b2b.product.dto.ProductFilter;
import com.ecom.b2b.product.model.Product;
import com.ecom.b2b.product.service.ProductService;

@Service
public class ProductServiceImpl extends AbstractService<ProductDTO, Product> implements ProductService {

	@Autowired
	private ProductDAO productDAO;

	@Override
	@Cacheable("products")
	public Page<ProductDTO> getByFilter(ProductFilter filter) {
		Specification<Product> specification = ProductDAOSpecificationBuilder.build(filter);
		Pageable pageable = getPagedRequest(filter);
		Page<Product> page = this.productDAO.findAll(specification, pageable);
		return convertEntityPageToDTOPage(page, pageable);
	}

	@Override
	@Cacheable(value="products", key="#id")
	public ProductDTO getById(String id) {
		System.out.println("-----------------------");
		Optional<Product> productOptional = this.productDAO.findById(id);
		if (!productOptional.isPresent()) {

		}
		final Product product = productOptional.get();
		return getDtoForEntity(product);
	}

	@Override
	@Cacheable(value="products", key="#sku")
	public ProductDTO getBySku(String sku) {
		Product product = this.productDAO.findBySkuAndIsActive(sku, true);
		if (product == null) {

		}
		return getDtoForEntity(product);
	}

	@Override
	public ProductDTO getDtoForEntity(Product e) {
		return new ProductDTO(e);
	}

	@Override
	public List<ProductDTO> getDTOsForEntities(Iterable<Product> entities) {
		return StreamSupport.stream(entities.spliterator(), false).map(element -> new ProductDTO(element))
				.collect(Collectors.toList());
	}

	@Override
	@CacheEvict(value = "products", key = "#id") 
	public ProductDTO create(ProductDTO t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@CacheEvict(value = "products", key = "#id") 
	public ProductDTO update(ProductDTO t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@CacheEvict(value = "products", key = "#id") 
	public void delete(String id) {
		// TODO Auto-generated method stub

	}

}
