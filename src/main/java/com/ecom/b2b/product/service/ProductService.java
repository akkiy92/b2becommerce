package com.ecom.b2b.product.service;

import org.springframework.data.domain.Page;

import com.ecom.b2b.product.dto.ProductDTO;
import com.ecom.b2b.product.dto.ProductFilter;

public interface ProductService  {
	
	ProductDTO create(ProductDTO t);
	
	ProductDTO update(ProductDTO t);
	
	void delete(String id);

	Page<ProductDTO> getByFilter(ProductFilter filter);

	ProductDTO getById(String id);

	ProductDTO getBySku(String sku);
}
