package com.ecom.b2b.product.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import com.ecom.b2b.brand.model.Brand;
import com.ecom.b2b.product.dto.ProductFilter;
import com.ecom.b2b.product.model.Product;
import com.ecom.b2b.supplier.model.Supplier;

/**
 * @author akshaysoni
 *
 */
public class ProductDAOSpecificationBuilder {

	/**
	 * @param filter
	 * @return
	 */
	public static Specification<Product> build(ProductFilter filter) {
		final ProductDAOSpecificationBuilder builder = new ProductDAOSpecificationBuilder();
		return builder.generateSpecification(filter);
	}

	/**
	 * @param filter
	 * @return
	 */
	private Specification<Product> generateSpecification(ProductFilter filter) {
		final List<Specification<Product>> specifications = new ArrayList<>();
		if (!StringUtils.isEmpty(filter.getBrandId())) {
			specifications.add(getBrandSpecification(filter.getBrandId()));
		}
		if (!StringUtils.isEmpty(filter.getColor())) {
			specifications.add(getColorSpecification(filter.getColor()));
		}
		if (filter.getSize() != null) {
			specifications.add(getSizeSpecification(filter.getSize()));
		}
		if (filter.getMinPrice() != null) {
			specifications.add(getMinPriceSpecification(filter.getMinPrice()));
		}
		if (filter.getMaxPrice() != null) {
			specifications.add(getMaxPriceSpecification(filter.getMaxPrice()));
		}
		if (!StringUtils.isEmpty(filter.getSku())) {
			specifications.add(getSkuSpecification(filter.getSku()));
		}
		if (!StringUtils.isEmpty(filter.getSupplierId())) {
			specifications.add(getSupplierSpecification(filter.getSupplierId()));
		}

		final Iterator<Specification<Product>> itr = specifications.iterator();
		if (itr.hasNext()) {
			Specification<Product> spec = Specification.where(itr.next());
			while (itr.hasNext()) {
				spec = spec.and(itr.next());
			}
			return spec;
		}
		return null;
	}

	private Specification<Product> getSkuSpecification(String sku) {
		return (root, query, cb) -> {
			return cb.equal(root.get("sku"), sku);
		};
	}

	private Specification<Product> getColorSpecification(String color) {
		return (root, query, cb) -> {
			return cb.equal(root.get("color"), color);
		};
	}

	private Specification<Product> getSizeSpecification(Integer size) {
		return (root, query, cb) -> {
			return cb.equal(root.get("size"), size);
		};
	}

	private Specification<Product> getMinPriceSpecification(BigDecimal minPrice) {
		return (root, query, cb) -> {
			return cb.greaterThanOrEqualTo(root.get("price"), minPrice);
		};
	}

	private Specification<Product> getMaxPriceSpecification(BigDecimal maxPrice) {
		return (root, query, cb) -> {
			return cb.lessThanOrEqualTo(root.get("price"), maxPrice);
		};
	}

	private Specification<Product> getBrandSpecification(String brandId) {
		return (root, query, cb) -> {
			Join<Product, Brand> brandJoin = root.join("brand", JoinType.LEFT);
			return cb.equal(brandJoin.get("id"), brandId);
		};
	}

	private Specification<Product> getSupplierSpecification(String supplierId) {
		return (root, query, cb) -> {
			Join<Product, Supplier> supplierJoin = root.join("supplier", JoinType.LEFT);
			return cb.equal(supplierJoin.get("id"), supplierId);
		};
	}

}
