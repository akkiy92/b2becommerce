package com.ecom.b2b.product.dao;

import com.ecom.b2b.core.repository.AbstractDAO;
import com.ecom.b2b.product.model.Product;

public interface ProductDAO extends AbstractDAO<Product> {
	Product findBySkuAndIsActive(String sku, boolean isActive);
}
