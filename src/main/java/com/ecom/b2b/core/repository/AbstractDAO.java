package com.ecom.b2b.core.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.ecom.b2b.core.model.BaseEntity;

@NoRepositoryBean
public interface AbstractDAO<T extends BaseEntity>
		extends PagingAndSortingRepository<T, String>, JpaSpecificationExecutor<T> {

}
