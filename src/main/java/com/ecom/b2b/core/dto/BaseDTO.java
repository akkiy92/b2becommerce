package com.ecom.b2b.core.dto;

import javax.validation.constraints.Size;

public class BaseDTO {

	@Size(min = 10, max = 50, message = "Id should be of between 10-50 characters.")
	private String id;

	public BaseDTO() {
		super();
	}

	public BaseDTO(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}
