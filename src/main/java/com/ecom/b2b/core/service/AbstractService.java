package com.ecom.b2b.core.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.ecom.b2b.core.filter.BaseFilter;
import com.ecom.b2b.core.model.BaseEntity;

public abstract class AbstractService<T, E extends BaseEntity> {

	public abstract T getDtoForEntity(E e);

	public abstract List<T> getDTOsForEntities(Iterable<E> entities);

	public Page<T> convertEntityPageToDTOPage(Page<E> page, Pageable pageable) {
		if (page != null) {
			List<T> dtos = getDTOsForEntities(page.getContent());
			return new PageImpl<T>(dtos, pageable, page.getTotalElements());
		} else {
			return new PageImpl<T>(new ArrayList<T>());
		}
	}

	public Pageable getPagedRequest(BaseFilter filter) {
		int pageIndex = filter.getPageIndex() != null ? filter.getPageIndex() : 0;
		int pageSize = filter.getPageSize() != null ? filter.getPageSize() : 10;
		Sort sort = null;
		if (filter.getSortOrder() == null) {
			sort = Sort.unsorted();
		} else {
			switch (filter.getSortOrder()) {
			case ASCENDING: {
				sort = Sort.by(Direction.ASC, filter.getSortField());
				break;
			}
			case DESCENDING: {
				sort = Sort.by(Direction.DESC, filter.getSortField());
				break;
			}
			default: {
				sort = Sort.by(Direction.ASC, filter.getSortField());
				break;
			}
			}
		}
		return PageRequest.of(pageIndex, pageSize, sort);
	}

}
