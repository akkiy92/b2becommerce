package com.ecom.b2b.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class CacheService {
	
	@Autowired
	private CacheManager cacheManager;

	public void evictAllCaches() {
		cacheManager.getCacheNames().stream().forEach(cacheName -> cacheManager.getCache(cacheName).clear());
	}

	@Scheduled(fixedRateString = "${cache.realod.time:100000}")
	public void evictAllcachesAtIntervals() {
		System.out.println("Cache Clear");
		evictAllCaches();
	}
}
