package com.ecom.b2b.core.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@MappedSuperclass
public abstract class BaseEntity implements Serializable {
	private static final long serialVersionUID = -702385096294911737L;

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	private String id;

	public BaseEntity() {
		this.id = UUID.randomUUID().toString();
	}

	@Override
	public int hashCode() {
		final int prime1 = 101;
		final int prime2 = 203;

		return new HashCodeBuilder(prime1, prime2).append(id).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		BaseEntity other = (BaseEntity) obj;
		return new EqualsBuilder().appendSuper(super.equals(obj)).append(id, other.id).isEquals();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}