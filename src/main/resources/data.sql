insert into brand values ('fea321ea-2ad6-4c63-82d0-de465f4e78d1', 'Levis', 'system', 'system', now(), now(), true);
insert into brand values ('e86f482a-fbfc-4ad0-98ee-89bcafe420f1', 'Puma', 'system', 'system', now(), now(), true);
insert into brand values ('8b0a02be-ef7f-4425-933a-0ae97756bbe2', 'Reebok', 'system', 'system', now(), now(), true);


insert into product_type values ('fea321ea-2ad6-4c63-82d0-de465f4e78d1', 'Shirt', 'system', 'system', now(), now(), true);
insert into product_type values ('e86f482a-fbfc-4ad0-98ee-89bcafe420f1', 'Pent', 'system', 'system', now(), now(), true);
insert into product_type values ('8b0a02be-ef7f-4425-933a-0ae97756bbe2', 'T-Shirt', 'system', 'system', now(), now(), true);

insert into supplier values ('fea321ea-2ad6-4c63-82d0-de465f4e78d1', 'Amazon', 'system', 'system', now(), now(), true);
insert into supplier values ('e86f482a-fbfc-4ad0-98ee-89bcafe420f1', 'Flipkart', 'system', 'system', now(), now(), true);
insert into supplier values ('8b0a02be-ef7f-4425-933a-0ae97756bbe2', 'Myntra', 'system', 'system', now(), now(), true);

insert into product values ('fea321ea-2ad6-4c63-82d0-de465f4e78d1', 'Roadster Solid V T-Shirt', 'fea321ea-2ad6-4c63-82d0-de465f4e78d1', '8b0a02be-ef7f-4425-933a-0ae97756bbe2', 400.00, 32, 'red', '8b0a02be-ef7f-4425-933a-0ae97756bbe2', 20, true, now(), now(), 'system', 'system', 'SKU-1');
insert into product values ('e86f482a-fbfc-4ad0-98ee-89bcafe420f1', 'WROGN T-Shirt', 'e86f482a-fbfc-4ad0-98ee-89bcafe420f1', '8b0a02be-ef7f-4425-933a-0ae97756bbe2', 299.00, 34, 'white', '8b0a02be-ef7f-4425-933a-0ae97756bbe2', 50, true, now(), now(), 'system', 'system', 'SKU-2');
insert into product values ('8b0a02be-ef7f-4425-933a-0ae97756bbe2', 'Shirt', 'e86f482a-fbfc-4ad0-98ee-89bcafe420f1', '8b0a02be-ef7f-4425-933a-0ae97756bbe2', 199.00, 28, 'yellow', '8b0a02be-ef7f-4425-933a-0ae97756bbe2', 100, true, now(), now(), 'system', 'system', 'SKU-3');