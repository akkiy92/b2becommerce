DROP TABLE IF EXISTS brand;
CREATE TABLE `brand` (
  `id` VARCHAR(50) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `created_by` VARCHAR(50) NOT NULL,
  `modified_by` VARCHAR(50) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `modified_at` DATETIME NOT NULL,
  `is_active` BIT(1) NOT NULL,
  PRIMARY KEY (`id`));


DROP TABLE IF EXISTS supplier;
CREATE TABLE `supplier` (
  `id` VARCHAR(50) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `created_by` VARCHAR(50) NOT NULL,
  `modified_by` VARCHAR(50) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `modified_at` DATETIME NOT NULL,
  `is_active` BIT(1) NOT NULL,
  PRIMARY KEY (`id`));

DROP TABLE IF EXISTS product_type;
CREATE TABLE `product_type` (
  `id` VARCHAR(50) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `created_by` VARCHAR(50) NOT NULL,
  `modified_by` VARCHAR(50) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `modified_at` DATETIME NOT NULL,
  `is_active` BIT(1) NOT NULL,
  PRIMARY KEY (`id`));

DROP TABLE IF EXISTS PRODUCT;
CREATE TABLE `product` (
  `id` VARCHAR(50) NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `brand_id` VARCHAR(50) NOT NULL,
  `type_id` VARCHAR(50) NOT NULL,
  `price` DECIMAL(11) NOT NULL,
  `size` INT NULL,
  `color` VARCHAR(20) NULL,
  `supplier_id` VARCHAR(50) NOT NULL,
  `available_qty` BIGINT(11) NOT NULL,
  `is_active` BIT(1) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `modified_at` DATETIME NOT NULL,
  `created_by` VARCHAR(50) NOT NULL,
  `modified_by` VARCHAR(50) NOT NULL,
  `SKU` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`));

